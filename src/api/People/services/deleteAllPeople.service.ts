import { getConnection } from "../../../database";

const deleteAllPeopleQuery = `
DELETE FROM people;
`;

const deleteAllPeople = async (id?: number) => {
  const db = await getConnection({ multipleStatements: true });

  await db.query({
    sql: deleteAllPeopleQuery,
    values: [id]
  });

  db.release();
};

export default deleteAllPeople;
